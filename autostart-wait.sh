#!/bin/bash

sleep 6s
fcitx5 2>&1 > /dev/null &
nutstore 2>&1 > /dev/null &
blueberry-tray 2>&1 > /dev/null &
nm-applet 2>&1 > /dev/null &
mate-power-manager 2>&1 > /dev/null &
/usr/lib/notification-daemon-1.0/notification-daemon 2>&1 > /dev/null &
noisetorch -i 2>&1 > /dev/null &
lxqt-policykit-agent 2>&1 > /dev/null &

/bin/bash ./autostart-self.sh &