#!/bin/sh
SCRIPTDIR=$(cd $(dirname "${BASH_SOURCE[0]}") >/dev/null && pwd)
xmenu <<EOF | sh &
Applications
	IMG:${SCRIPTDIR}/icons/firefox.png	firefox	firefox
	IMG:${SCRIPTDIR}/icons/chrome.png	Chrome	chromium
	IMG:${SCRIPTDIR}/icons/gimp.png	Image editor	gimp
	IMG:${SCRIPTDIR}/icons/steam.png	Steam	steam
Terminal (st)		st
nmtui		nmtui
arandr		arandr
pavucontrol		pavucontrol
EOF
