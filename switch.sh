#!/bin/bash

xrandr --output DP-2  --mode 2560x1440 --dpi 96 --scale 1x1 --above eDP-1 --output eDP-1 --auto
xrandr --output eDP-1 --off
xrandr --output DP-2  --mode 2560x1440 --dpi 96 --scale 1.5x1.5
