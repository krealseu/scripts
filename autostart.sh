#!/bin/bash

# xrdb ~/.Xresources
# /bin/bash ./wp-change-auto.sh &
# /bin/bash ./dwm-status.sh &
status &
xautolock -time 15 -locker slock &
picom --experimental-backends --config ~/Documents/scripts/config/picom.conf -b
# picom -b
setxkbmap -option caps:swapescape
# xrandr --output DP-2  --mode 2560x1440 --dpi 96 --scale 1.5x1.5
# systemctl enable bluetooth 
/bin/bash ./autostart-wait.sh &

export _JAVA_AWT_WM_NONREPARENTING=1 
export AWT_TOOLKIT=MToolkit 
wmname LG3D

